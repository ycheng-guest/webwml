<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In 389-ds-base up to version 1.4.1.2, requests were handled by worker
threads. Each socket had been waited for by the worker for at most
<q>ioblocktimeout</q> seconds. However, this timeout applied only to
un-encrypted requests. Connections using SSL/TLS were not taking this
timeout into account during reads, and may have hung longer. An
unauthenticated attacker could have repeatedly created hanging LDAP
requests to hang all the workers, resulting in a Denial of Service.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.3.5-4+deb8u6.</p>

<p>We recommend that you upgrade your 389-ds-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1779.data"
# $Id: $
