<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several denial-of-service vulnerabilities (assertion failures) were
discovered in BIND, a DNS server implementation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9131">CVE-2016-9131</a>

     <p>A crafted upstream response to an ANY query could cause an
     assertion failure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9147">CVE-2016-9147</a>

     <p>A crafted upstream response with self-contradicting DNSSEC data
     could cause an assertion failure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9444">CVE-2016-9444</a>

     <p>Specially-crafted upstream responses with a DS record could cause
     an assertion failure.</p></li>

</ul>

<p>These vulnerabilities predominantly affect DNS servers providing
recursive service.  Client queries to authoritative-only servers
cannot trigger these assertion failures.  These vulnerabilities are
present whether or not DNSSEC validation is enabled in the server
configuration.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:9.8.4.dfsg.P1-6+nmu2+deb7u14.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-805.data"
# $Id: $
