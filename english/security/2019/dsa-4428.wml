<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Jann Horn discovered that the PAM module in systemd insecurely uses the
environment and lacks seat verification permitting spoofing an active
session to PolicyKit. A remote attacker with SSH access can take
advantage of this issue to gain PolicyKit privileges that are normally
only granted to clients in an active session on the local console.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 232-25+deb9u11.</p>

<p>This update includes updates previously scheduled to be released in the
stretch 9.9 point release.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>For the detailed security status of systemd please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/systemd">https://security-tracker.debian.org/tracker/systemd</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4428.data"
# $Id: $
