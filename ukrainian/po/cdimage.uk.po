# translation of templates.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@debian.org>, 2004, 2005, 2006.
msgid ""
msgstr ""
"Project-Id-Version: templates\n"
"PO-Revision-Date: 2006-10-07 19:32+0200\n"
"Last-Translator: Eugeniy Meshcheryakov <eugen@debian.org>\n"
"Language-Team: Ukrainian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "      Відбиток ключа"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr ""

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr ""

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />FAQ"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Завантажити за допомогою Jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Завантажити через HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Купити CD або DVD"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Встановлення через мережу"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Завантажити"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />Різне"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr ""

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Відображення"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Rsync-дзеркала"

#: ../../english/template/debian/cdimage.wml:43
#, fuzzy
#| msgid "<void id=\"dc_misc\" />Misc"
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_misc\" />Різне"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Завантажити через Torrent"

#: ../../english/template/debian/cdimage.wml:49
msgid "<void id=\"dc_relinfo\" />Image Release Info"
msgstr ""

#: ../../english/template/debian/cdimage.wml:52
msgid "Debian CD team"
msgstr "Команда CD Debian"

#: ../../english/template/debian/cdimage.wml:55
msgid "debian_on_cd"
msgstr "Debian на CD"

#: ../../english/template/debian/cdimage.wml:58
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />FAQ"

#: ../../english/template/debian/cdimage.wml:61
msgid "jigdo"
msgstr "jidgo"

#: ../../english/template/debian/cdimage.wml:64
msgid "http_ftp"
msgstr "http_ftp"

#: ../../english/template/debian/cdimage.wml:67
msgid "buy"
msgstr "купити"

#: ../../english/template/debian/cdimage.wml:70
msgid "net_install"
msgstr "встановлення через мережу"

#: ../../english/template/debian/cdimage.wml:73
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />різне"

#: ../../english/template/debian/cdimage.wml:76
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"Англомовний <a href=\"/MailingLists/disclaimer\">список розсилки</a> "
"присвячений CD/DVD:"
