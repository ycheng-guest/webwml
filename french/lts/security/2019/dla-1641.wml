#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs conditions d’épuisement de pile ont été trouvées dans mxml qui
pourrait facilement planter lors de l’analyse de fichiers xml.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4570">CVE-2016-4570</a>

<p>La fonction mxmlDelete dans mxml-node.c permet à des attaquants distants de
provoquer un déni de service (consommation de pile) à l’aide d’un fichier xml
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4571">CVE-2016-4571</a>

<p>La fonction mxml_write_node dans mxml-file.c permet à des attaquants distants
de provoquer un déni de service (consommation de pile) à l’aide d’un fichier xml
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20004">CVE-2018-20004</a>

<p>Un dépassement de pile dans mxml_write_node à l’aide de vecteurs
impliquant un nombre à virgule flottante de double précision.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2.6-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mxml.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1641.data"
# $Id: $
