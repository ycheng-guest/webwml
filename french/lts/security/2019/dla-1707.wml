#use wml::debian::translation-check translation="04be8147985325ac50d62fdf5a373d5eb24a458c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans symfony, un
cadriciel pour application web de PHP. De nombreux composants de symfony sont
affectés : Security, lecteurs de groupe, gestion de session, SecurityBundle,
HttpFoundation, Form et Security\Http.</p>

<p>Les alertes de l’amont correspondantes fournissent plus de détails :</p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2017-16652">CVE-2017-16652</a>]
<a href="https://symfony.com/blog/cve-2017-16652-open-redirect-vulnerability-on-security-handlers">https://symfony.com/blog/cve-2017-16652-open-redirect-vulnerability-on-security-handlers</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2017-16654">CVE-2017-16654</a>]
<a href="https://symfony.com/blog/cve-2017-16654-intl-bundle-readers-breaking-out-of-paths">https://symfony.com/blog/cve-2017-16654-intl-bundle-readers-breaking-out-of-paths</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-11385">CVE-2018-11385</a>]
<a href="https://symfony.com/blog/cve-2018-11385-session-fixation-issue-for-guard-authentication">https://symfony.com/blog/cve-2018-11385-session-fixation-issue-for-guard-authentication</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-11408">CVE-2018-11408</a>]
<a href="https://symfony.com/blog/cve-2018-11408-open-redirect-vulnerability-on-security-handlers">https://symfony.com/blog/cve-2018-11408-open-redirect-vulnerability-on-security-handlers</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-14773">CVE-2018-14773</a>]
<a href="https://symfony.com/blog/cve-2018-14773-remove-support-for-legacy-and-risky-http-headers">https://symfony.com/blog/cve-2018-14773-remove-support-for-legacy-and-risky-http-headers</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-19789">CVE-2018-19789</a>]
<a href="https://symfony.com/blog/cve-2018-19789-disclosure-of-uploaded-files-full-path">https://symfony.com/blog/cve-2018-19789-disclosure-of-uploaded-files-full-path</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-19790">CVE-2018-19790</a>]
<a href="https://symfony.com/blog/cve-2018-19790-open-redirect-vulnerability-when-using-security-http">https://symfony.com/blog/cve-2018-19790-open-redirect-vulnerability-when-using-security-http</a></p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2.3.21+dfsg-4+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets symfony.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1707.data"
# $Id: $
