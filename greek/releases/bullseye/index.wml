#use wml::debian::template title="Πληροφορίες έκδοσης Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b" maintainer="galaxico"

<if-stable-release release="bullseye">

<p>Η έκδοση του Debian <current_release_bullseye> 
κυκλοφόρησε στις <current_release_date_bullseye>.
<ifneq "11.0" "<current_release>"
  "Το Debian 11.0 κυκλοφόρησε αρχικά στις <:=spokendate('XXXXXXXX'):>."
/>
Η έκδοση περιελάμβανε πολλές μείζονες αλλαγές που περιγράφονται στην <a 
href="$(HOME)/News/XXXX/XXXXXXXX">Ανακοίνωση τύπου</a> και τις  <a 
href="releasenotes">Σημειώσεις της Έκδοσης</a>.</p>

<p>Για να αποκτήσετε και να εγκαταστήσετε το Debian, δείτε τη σελίδα <a 
href="debian-installer/">πληροφορίες εγκατάστασης</a> και τον 
<a href="installmanual">Οδηγό Εγκατάστασης</a>. Για να αναβαθμίσετε από μια 
παλιότερη έκδοση του Debian, δείτε τις οδηγίες στις
<a href="releasenotes">Σημειώσεις της Έκδοσης</a>.</p>

<p>Στην παρούσα έκδοση υποστηρίζονται οι ακόλουθες αρχιτεκτονικές:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Παρά την επιθυμία μας, ίσως υπάρχουν μερικά προβλήματα σ' αυτή την έκδοση, 
έστω και αν έχει κηρυχθεί <em>σταθερή</em>. Έχουμε ετοιμάσει μια  
<a href="errata">λίστα με τα κυριότερα γνωστά προβλήματα</a>, και πάντα 
μπορείτε να 
<a href="reportingbugs">αναφέρετε άλλα ζητήματα</a> σε μας.</p>

<p>Τέλος, αλλά όχι με λιγότερη σημασία, έχουμε μια λίστα με <a 
href="credits">τα άτομα τα οποία παίρνουν τα εύσημα</a> για την 
πραγματοποίηση αυτής της έκδοσης.</p>
</if-stable-release>

<if-stable-release release="buster">

<p>Η κωδική ονομασία της επόμενης μείζονος έκδοσης του Debian μετά την <a
href="../buster/">buster</a> είναι <q>bullseye</q>.</p>

<p>Αυτή η έκδοση ξεκίνησε ως ένα αντίγραφο της έκδοσης buster, και βρίσκεται 
αυτή τη στιγμή σε μια κατάσταση που λέγεται <q><a 
href="$(DOC)/manuals/debian-faq/ch-ftparchives#s-testing">δοκιμαστική 
(testing)</a></q> .
Αυτό σημαίνει ότι τα πράγματα δεν ενδέχεται να "χαλάσουν" τόσο άσχημα όσο 
με την ασταθή ή την πειραματική διανομή, επειδή τα πακέτα επιτρέπεται 
να μπουν στη διανομή αυτή μόνο μετά από το πέρασμα μιας συγκεκριμένης 
χρονικής περιόδου και εφόσον δεν έχουν υποβληθεί γι' αυτά αναφορές με σφάλματα 
κρίσιμης για την έκδοση σοβαρότητας.</p>

<p>Παρακαλούμε σημειώστε ότι η ομάδα ασφαλείας <strong>δεν</strong> 
διαχειρίζεται ακόμα αναβαθμίσεις ασφαλείας για τη <q>δοκιμαστική (testing)</q> 
διανομή. Συνεπώς, η <q>δοκιμαστική</q> διανομή <strong>δεν</strong> λαμβάνει 
έγκαιρα αναβαθμίσεις ασφαλείας.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
Σας ενθαρρύνουμε, αν χρειάζεστε αναβαθμίσεις ασφαλείας, να αλλάξετε, προς το 
παρόν, το αρχείο sources.list από τη δοκιμαστική στη σταθερή διανομή (buster). 
Δείτε επίσης την αντίστοιχη γραμμή στη σελίδα 
<a href="$(HOME)/security/faq#testing">Συχνών ερωτήσεων της Ομάδας 
ασφαλείας</a> για τη <q>δοκιμαστική</q> διανομή.</p>

<p>Ίσως υπάρχει διαθέσιμη μια <a href="releasenotes">προκαταρκτική έκδοση των 
σημειώσεων της έκδοσης</a>.
Παρακαλούμε επίσης  <a 
href="https://bugs.debian.org/release-notes">ελέγξτε τις προτεινόμενες 
προσθήκες στις σημειώσεις της έκδοσης</a>.</p>

<p>Για εικόνες αρχείων της εγκατάστασης και τεκμηρίωση σχετικά με το πώς να 
εγκαταστήσετε τη 
<q>δοκιμαστική</q> έκδοση, δείτε τη σελίδα του  <a 
href="$(HOME)/devel/debian-installer/">Εγκαταστάτη του Debian</a>.</p>

<p>Για περισσότερα σχετικά με το πώς δουλεύει η <q>δοκιμαστική</q> 
διανομή,ελέγξτε τις
<a href="$(HOME)/devel/testing">πληροφορίες των προγραμματιστών/στριών 
σχετικά με αυτήν</a>.</p>

<p>Κόσμος ρωτάει συχνά αν υπάρχει ένα μοναδικός <q>μετρητής προόδου</q> για την 
έκδοση. Δυστυχώς, κάτι τέτοιο δεν υπάρχει, αλλά μπορούμε να σας παραπέμψουμε σε 
αρκετά σημεία που περιγράφουν ζητήματα που πρέπει να αντιμετωπιστούν ώστε να 
μπορέσει να πραγματοποιηθεί η κυκλοφορία αυτής της έκδοσης:</p>

<ul>
  <li><a href="https://release.debian.org/">σελίδα με τη γενική 
κατάσταση της έκδοσης</a></li>
  <li><a 
href="https://bugs.debian.org/release-critical/">Σφάλματα 
κρίσιμης σοβαρότητας για την έκδοση (Release-critical bugs)</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Σφάλματα 
του βασικού συστήματος</a></li>
  <li><a 
href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Σφάλματα για 
τα συνηθισμένα πακέτα και τα πακέτα ομάδων λογισμικού (task packages)</a></li>
</ul>

<p>Επιπλέον, αναφορές για τη γενική κατάσταση της διανομής εκδίδονται από τον 
διαχειριστή της έκδοσης στη λίστα αλληλογραφίας <a 
href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce</a>.</p>

</if-stable-release>
